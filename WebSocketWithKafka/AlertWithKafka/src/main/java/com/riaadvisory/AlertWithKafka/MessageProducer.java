package com.riaadvisory.AlertWithKafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Component
//@EnableScheduling
@RestController
@RequestMapping("/kafka-test")
public class MessageProducer {

	@Value("${kafka.input.topic}")
	private String kafkaInputTopic;

	@Autowired
	private GreetingService greetingService;

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

//	@Scheduled(fixedRate = 1000)
	@GetMapping("/produce")
	public void produce() {
		String msg = greetingService.greet();

		System.out.println("Greeting Message :: " + msg);

		kafkaTemplate.send(kafkaInputTopic, msg);
	}

}