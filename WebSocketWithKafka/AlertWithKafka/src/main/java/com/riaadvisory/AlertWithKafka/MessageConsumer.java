package com.riaadvisory.AlertWithKafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
@Component
public class MessageConsumer {

    @Value("${stomp.topic}")
    private String stompTopic;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    private static  Map<String, List<Object>> userMessageMapping;

    @KafkaListener(topics = "${kafka.output.topic}", groupId = "${spring.kafka.consumer.group-id}")
    public void consumeMessage(String msg) {
        if (msg.contains("Good Afternoon")) {
            System.out.println("Message received For EPM : " + msg);
        } else
            System.out.println("Message received For TA : " + msg);
        messagingTemplate.convertAndSend(stompTopic, msg);
    }

}