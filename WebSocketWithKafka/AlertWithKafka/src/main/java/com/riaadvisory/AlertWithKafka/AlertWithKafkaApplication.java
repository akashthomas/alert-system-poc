package com.riaadvisory.AlertWithKafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlertWithKafkaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlertWithKafkaApplication.class, args);
	}

}
