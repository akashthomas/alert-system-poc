import { Injectable } from '@angular/core';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';
import { WEBSOCKET_ENDPOINT, WEBSOCKET_NOTIFY_TOPIC } from '../constant/UrlConstant';
import { NotificationService } from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  constructor(private notificationService: NotificationService) { }

  stompClient: any;

  connect(): void {
    const ws = new SockJS(WEBSOCKET_ENDPOINT);
    this.stompClient = Stomp.over(ws);
    this.stompClient.connect({}, (frame) => {
      this.stompClient.subscribe(WEBSOCKET_NOTIFY_TOPIC, (sdkEvent) => {
        this.onMessageReceived(sdkEvent)
      });

    }, this.errorCallBack);


  }
  onMessageReceived(message) {
    console.log('Message Recieved from Server :: ' + message);
    this.notificationService.notificationMessage.emit(message.body);
  }

  errorCallBack(error) {
    console.log('errorCallBack -> ' + error);
    setTimeout(() => {
      this.connect();
    }, 5000);
  }


  disconnect(): void {
    if (this.stompClient !== null) {
      this.stompClient.disconnect();
    }
    console.log('Disconnected');
  }

}

