import { Component, OnInit } from '@angular/core';
import { IdentitySerializer, JsonSerializer, RSocket, RSocketClient } from 'rsocket-core';
import RSocketWebSocketClient from 'rsocket-websocket-client';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
 //
  name;

  submit(){
    console.log("sending msg");

    this.client.send("/app/hello", {}, JSON.stringify(this.name));
  }
 //


  url = 'http://localhost:8889/rsocket'
  // channel = 'my-request-response';
  channel = '/topic/greetings';
  client: RSocketClient;
  greeting: string;

  ngOnInit() {
    // this.title.setTitle('Angular Spring Websocket');

    this.client = new RSocketClient({
      serializers: {
        data: JsonSerializer,
        metadata: IdentitySerializer
      },
      setup: {
        keepAlive: 60000,
        lifetime: 180000,
        dataMimeType: 'application/json',
        metadataMimeType: 'message/x.rsocket.routing.v0',
      },
      transport: new RSocketWebSocketClient({
        url: 'ws://localhost:8889/rsocket'
      }),
    });
    // error handler
    const errorHanlder = (e) => console.log(e);
    // response handler
    const responseHanlder = (payload) => {
      const li = document.createElement('li');
      li.innerText = payload.data;
      li.classList.add('list-group-item', 'small')
      document.getElementById('result').appendChild(li);
    }
    // request to rsocket-websocket and response handling
    const numberRequester = (socket, value) => {
      socket.requestStream({
        data: {'userId': "akash"},
        metadata: String.fromCharCode(this.channel.length) + this.channel
      }).subscribe({
        onError: errorHanlder,
        onNext: responseHanlder,
        onSubscribe: subscription => {
          subscription.request(100); // set it to some max value
        }
      })
    }
    // this.client.connect().then(socket => {
    //   document.getElementById('n').addEventListener('change', ({ srcElement }) => {
    //     numberRequester(socket, parseInt(srcElement.value));
    //   })
    // }, errorHanlder);

  //   this.client.connect().then(sock => {

  //     document.getElementById('n').addEventListener('keyup', ({srcElement}) => {
  //         if(srcElement.value.length > 0){

  //         }
  //     })
  // }, errorHanlder);
    this.client.connect().subscribe({
      onComplete: (socket: RSocket) => {
        socket
          .requestStream({
            data: {'userId': "akash"},
            metadata: String.fromCharCode(this.channel.length) + this.channel
          })
          .subscribe({
            onComplete: () => console.log('complete'),
            onError: (error: string) => {
              console.log('Connection has been closed due to:: ' + error);
            },
            onNext: (payload: { data: any; }) => {
              console.log(payload);
            },
            onSubscribe: (subscription: { request: (arg0: number) => void; }) => {
              subscription.request(1000000);
            },
          });
      },
      onError: (error: string) => {
        console.log('Connection has been refused due to:: ' + error);
      },
      onSubscribe: () => { }
    });
  }

}
