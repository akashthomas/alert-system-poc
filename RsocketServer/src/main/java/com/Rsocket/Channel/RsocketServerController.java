package com.Rsocket.Channel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
//import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Controller
public class RsocketServerController {
    @Autowired
    SimpMessagingTemplate template;
    private static final Map<String, Notification> notifications = new HashMap<String, Notification>() {
        {
            put("akash", new Notification("Batch", "EPM", "Batch failed for pricelist", "1"));
            put("wss.demo", new Notification("Batch", "EPM", "Batch success for pricelist", "2"));
            put("riademo2", new Notification("TA", "EPM", "Party hierarchy failed", "3"));
        }
    };
    private final Flux<String> stream;
    Logger logger = LoggerFactory.getLogger(RsocketServerController.class);

    @Autowired
    public RsocketServerController(final Flux<String> stream) {
        this.stream = stream;
    }

    @MessageMapping("my-request-response")
    public Flux<Notification> requestResponse(Notification notification) {
        logger.info("Received notification for my-request-response: " + notification);
        return Flux
                .interval(Duration.ZERO, Duration.ofSeconds(1))
                .map(not -> notifications.get(notification.getUserId()));
    }

    @MessageMapping("my.time-updates.stream")
    public Flux<Notification> getTimeUpdatesStream(Flux<String> userId) {
//        return numberFlux
//                .map(n -> n * n)
//                .onErrorReturn(-1L);

        logger.info("Received notification for channel: " + userId);
        return userId.filter(us -> notifications.containsKey(us))
                .map(u -> notifications.get(u))
                .delayElements(Duration.ofSeconds(1));
    }

//    @MessageMapping("/hello")
//    @SendTo("/topic/greetings")
//    public String greeting(String user) throws Exception {
//        Thread.sleep(1000); // simulated delay
//        return "Hello "+ user;
//    }

}