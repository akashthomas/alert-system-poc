package com.Rsocket.Channel;

public class Notification {
    private String source;
    private String destination;
    private String text;
    private String userId;

    public Notification() {
        super();
    }
 
    public Notification(String source, String destination, String text, String userId) {
        this.source = source;
        this.destination = destination;
        this.text = text;
        this.userId = userId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "source='" + source + '\'' +
                ", destination='" + destination + '\'' +
                ", text='" + text + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }
}