import { Component } from '@angular/core';
import { SocketIoService } from './socket-io.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'socket-io-client';

  constructor(private socketIoService: SocketIoService) { }
  username: string = "Client";
  consoleData: string = `<p>Welcome</p>`;

  onConnect() {
    this.socketIoService.connect(this.username);

    this.listenConnect();
    this.listentDisconnect();
    this.listenOutput();
    this.listenFailure();
  }

  onDisconnect() {
    this.socketIoService.disconnect();
  }

  listenConnect(){
    this.socketIoService.listen("connect").subscribe(
      res => {
        console.log("connected");

        this.consoleData = this.consoleData + '<p>' + 'Connected to Server!' + '</p>';
      }
    );
  }

  listentDisconnect(){
    this.socketIoService.listen("disconnect").subscribe(
      res => {
        this.consoleData = this.consoleData + '<p>' + 'Disconnected!' + '</p>';
      }
    );
  }

  listenOutput(){
    this.socketIoService.listen("chat").subscribe(
      (data:any) => {
        console.log(data);

        this.consoleData = this.consoleData + '<p>' + data.userName + " : "+ data.message + '</p>';
      }
    );
  }

  listenFailure(){

      this.socketIoService.listen("connect_error").subscribe(
      (data:any) => {
        console.log(data);

        this.consoleData = this.consoleData + '<p>' + data.userName + " : "+ data.message + '</p>';
      }
    );
  }

  onConnectAlert() {
    this.socketIoService.connectToAlert(this.username);
  }

  onDisconnectAlert() {
    this.socketIoService.disconnectAlert();
  }
}
