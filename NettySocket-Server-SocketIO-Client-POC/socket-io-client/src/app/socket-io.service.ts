import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
// import { io, Socket } from 'socket.io-client';
// import * as io from '@socket.io-client';
// @ts-ignore
import io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class SocketIoService {

  readonly uri = ""
  server: any;

  constructor() {
  }
  socket!: any;
  socketAlert!: any;
  messages = ""

  connect(username: string) {
    this.socket = io("http://localhost:9092/chat" , { transports: ['polling', 'websocket'], query : {username}});
  }

  listen(eventName: string) {
    return new Observable(subscriber => {
      this.socket.on(eventName, (data: any) => {
        subscriber.next(data);
      });
    });
  }

  disconnect() {
    this.socket.disconnect();
  }

  // Alert

  connectToAlert(username: string) {
    this.socketAlert = io("http://localhost:9092/alert" , { transports: ['polling', 'websocket'], query : {username}});

    this.listenAlert("alert").subscribe(
        (data:any) => {
          console.log(data);
          alert(data.senderId + " : "+ data.message);
          // send back that alert has been received
          console.log("alert received");

          this.emitAlertReceived(true, username, data.id);

          // confirm(data.senderId + " : "+ data.message);
        }
      );
  }

  listenAlert(eventName: string) {
    return new Observable(subscriber => {
      this.socketAlert.on(eventName, (data: any) => {
        subscriber.next(data);
      });
    });
  }

  disconnectAlert() {
    this.socketAlert.disconnect();
  }

  emitAlertReceived(received:boolean, username:string, id: string){
    let data = {
      received,
      username,
      id
    }
    this.socketAlert.emit('alert', data);
  }

}
