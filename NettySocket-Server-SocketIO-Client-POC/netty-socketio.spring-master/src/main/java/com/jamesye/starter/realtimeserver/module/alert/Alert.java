package com.jamesye.starter.realtimeserver.module.alert;

import java.time.LocalDateTime;

public class Alert {
    private String id;
    private String message;
    private String type; //Notification/Snackbar/Message
    private String status; //Pending,Sent,Read
    private LocalDateTime createdOn;
    private LocalDateTime sentOn;
    private LocalDateTime readOn;
    private String senderId;
    private String receiverId;

    public Alert() {
    }

    public Alert(String id, String message, String type, String status, LocalDateTime createdOn, LocalDateTime sentOn, LocalDateTime readOn, String senderId, String receiverId) {
        this.id = id;
        this.message = message;
        this.type = type;
        this.status = status;
        this.createdOn = createdOn;
        this.sentOn = sentOn;
        this.readOn = readOn;
        this.senderId = senderId;
        this.receiverId = receiverId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public LocalDateTime getSentOn() {
        return sentOn;
    }

    public void setSentOn(LocalDateTime sentOn) {
        this.sentOn = sentOn;
    }

    public LocalDateTime getReadOn() {
        return readOn;
    }

    public void setReadOn(LocalDateTime readOn) {
        this.readOn = readOn;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }
}
