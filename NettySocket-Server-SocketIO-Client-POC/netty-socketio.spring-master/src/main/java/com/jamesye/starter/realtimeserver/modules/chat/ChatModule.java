package com.jamesye.starter.realtimeserver.modules.chat;

import com.corundumstudio.socketio.*;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DataListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import io.netty.util.internal.PlatformDependent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class ChatModule {

    private static final Logger log = LoggerFactory.getLogger(ChatModule.class);

    private final SocketIONamespace namespace;

    Map<String, String> userMap = new HashMap<>();

    @Autowired
    public ChatModule(SocketIOServer server) {
        this.namespace = server.addNamespace("/chat");
        this.namespace.addConnectListener(onConnected());
        this.namespace.addDisconnectListener(onDisconnected());
        this.namespace.addEventListener("chat", ChatMessage.class, onChatReceived());
    }

    private DataListener<ChatMessage> onChatReceived() {
        return (client, data, ackSender) -> {
            log.debug("Client[{}] - Received chat message '{}'", client.getSessionId().toString(), data);
            namespace.getBroadcastOperations().sendEvent("chat", data);
        };
    }

    private ConnectListener onConnected() {
        return client -> {
            HandshakeData handshakeData = client.getHandshakeData();
            String username = handshakeData.getSingleUrlParam("username");
            if(username != null)
            {
                userMap.put(handshakeData.getSingleUrlParam("username"), client.getSessionId().toString());
            }

            log.debug("Client[{}] - Connected to chat module through '{}'", client.getSessionId().toString(), handshakeData.getUrl());
        };
    }

    private DisconnectListener onDisconnected() {
        return client -> {
            log.debug("Client[{}] - Disconnected from chat module.", client.getSessionId().toString());
        };
    }

    public void broadcast(String data){
        ChatMessage cm = new ChatMessage();
        cm.setMessage(data);
        cm.setUserName("System");
        namespace.getBroadcastOperations().sendEvent("chat", cm);
    }

    public void broadcastTo(String username, String data){
        ChatMessage cm = new ChatMessage();
        cm.setMessage(data);
        cm.setUserName("System");
        namespace.getClient(UUID.fromString(userMap.get(username))).sendEvent("chat", cm);
    }

    public void broadcastToPerticularGroup(String groupname, String data){
        ChatMessage cm = new ChatMessage();
        cm.setMessage(data);
        cm.setUserName("System");
        Map<UUID, SocketIOClient> someClients = PlatformDependent.newConcurrentHashMap();
        namespace.getClient(UUID.fromString(userMap.get(groupname))).sendEvent("chat", cm);
    }
}
