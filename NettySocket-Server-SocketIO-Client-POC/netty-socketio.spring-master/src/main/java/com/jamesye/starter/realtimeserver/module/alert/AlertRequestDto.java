package com.jamesye.starter.realtimeserver.module.alert;

import java.util.List;

public class AlertRequestDto {

    private String senderId;
    private List<String> receiverId;
    private String message;
    private String type; //Alert/Snackbar

    public AlertRequestDto() {
    }

    public AlertRequestDto(String senderId, List<String> receiverId, String message, String type) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.message = message;
        this.type = type;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public List<String> getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(List<String> receiverId) {
        this.receiverId = receiverId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
