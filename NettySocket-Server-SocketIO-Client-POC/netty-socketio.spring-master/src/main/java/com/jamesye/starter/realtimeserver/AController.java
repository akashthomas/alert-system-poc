package com.jamesye.starter.realtimeserver;

import com.jamesye.starter.realtimeserver.modules.chat.ChatModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AController {

    @Autowired
    private ChatModule chatModule;

    @GetMapping("/hello")
    public String sayHello() {
        chatModule.broadcast("Hello!");
        return "Hello India";
    }

    @GetMapping("/hello/{message}")
    public String sayHelloWithMessage(@PathVariable String message) {
        chatModule.broadcast(message);
        return "Sent: "+message;
    }

    @GetMapping("/hello/{username}/{message}")
    public String sayHelloWithMessageWithUsername(@PathVariable String username,@PathVariable String message) {
        chatModule.broadcastTo(username,message);
        return "Sent to "+ username;
    }

}
