package com.jamesye.starter.realtimeserver.module.alert;

import com.corundumstudio.socketio.HandshakeData;
import com.corundumstudio.socketio.SocketIONamespace;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DataListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import com.jamesye.starter.realtimeserver.modules.chat.ChatMessage;
import com.jamesye.starter.realtimeserver.modules.chat.ChatModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class AlertModule {

    private static final Logger log = LoggerFactory.getLogger(ChatModule.class);

    private final SocketIONamespace namespace;

    Map<String, String> userMap = new HashMap<>();
    Map<String, ArrayList<Alert>> alertMap = new HashMap<>();

    @Autowired
    public AlertModule(SocketIOServer server) {
        this.namespace = server.addNamespace("/alert");
        this.namespace.addConnectListener(onConnected());
        this.namespace.addDisconnectListener(onDisconnected());
        this.namespace.addEventListener("alert", HashMap.class, onAlertRead());
    }

    private ConnectListener onConnected() {
        return client -> {
            HandshakeData handshakeData = client.getHandshakeData();
            String username = handshakeData.getSingleUrlParam("username");
            if(username != null)
            {
                userMap.put(handshakeData.getSingleUrlParam("username"), client.getSessionId().toString());
            }

            log.debug("Client[{}] - Connected to chat module through '{}'", client.getSessionId().toString(), handshakeData.getUrl());
        };
    }

    private DisconnectListener onDisconnected() {
        return client -> {
            log.debug("Client[{}] - Disconnected from chat module.", client.getSessionId().toString());
        };
    }

    private DataListener<HashMap> onAlertRead() {
        return (client, data, ackSender) -> {
            log.debug("Client[{}] - Received chat message '{}'", client.getSessionId().toString(), data);
//            namespace.getBroadcastOperations().sendEvent("chat", data);
            alertMap.get(data.get("username")).forEach(alert -> {
                if(alert.getId().equals(data.get("id"))){
                    alert.setReadOn(LocalDateTime.now());
                    alert.setStatus("READ");
                }
            });
        };
    }

    public void broadcastAlerts(AlertRequestDto alertRequestDto){
        alertRequestDto.getReceiverId().forEach(to -> {
            if(!alertMap.containsKey(to)){
                alertMap.put(to, new ArrayList<>());
            }
            Alert alert = new Alert(UUID.randomUUID().toString(),alertRequestDto.getMessage(),alertRequestDto.getType(),"SENT", LocalDateTime.now(), LocalDateTime.now(), null, alertRequestDto.getSenderId(),to);
            alertMap.get(to).add(alert);
            try{
                broadcastAlert(alert);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void alertToAll(){
        Alert alert = new Alert(UUID.randomUUID().toString(),"Welcome to RIA Alert Service","Alert","SENT", LocalDateTime.now(), LocalDateTime.now(), null, "RIA System",null);
        broadcastAlert(alert);
    }

    private void broadcastAlert(Alert alert){
        if(alert.getReceiverId() == null) {
            namespace.getBroadcastOperations().sendEvent("alert", alert);
        } else {
            namespace.getClient(UUID.fromString(userMap.get(alert.getReceiverId()))).sendEvent("alert", alert);
        }
    }

    public void broadcastPendingAlerts(){
        alertMap.forEach((key, alerts) -> {
            alerts.forEach(alert -> {
                if(!alert.getStatus().equals("READ"))
                {
                    try{
                        broadcastAlert(alert);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        });
    }
}
