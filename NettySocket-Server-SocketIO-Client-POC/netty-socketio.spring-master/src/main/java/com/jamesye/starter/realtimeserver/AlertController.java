package com.jamesye.starter.realtimeserver;

import com.jamesye.starter.realtimeserver.module.alert.AlertModule;
import com.jamesye.starter.realtimeserver.module.alert.AlertRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController("/alert")
public class AlertController {

    @Autowired
    private AlertModule alertModule;

    @PostMapping()
    public String sayHello(@RequestBody AlertRequestDto alertRequestDto) {

        alertModule.broadcastAlerts(alertRequestDto);
        return "Hello India";
    }

    @GetMapping()
    public String sayHello() {
        alertModule.alertToAll();
        return "Alert sent!";
    }

    @GetMapping("/job")
    public String runJob() {
        alertModule.broadcastPendingAlerts();
        return "job run successful";
    }
}
